package be.swing.basic;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.regex.Pattern;

public class MyKeyListener implements KeyListener {
    private static final Pattern pattern = Pattern.compile("[0-9a-z]");

    @Override
    public void keyTyped(KeyEvent e) {
        if (pattern.matcher(String.valueOf(e.getKeyChar())).matches()) {
            System.out.println("Forbidden character : " + e.getKeyChar());
            e.consume();
        }

    }

    @Override
    public void keyPressed(KeyEvent e) {
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
