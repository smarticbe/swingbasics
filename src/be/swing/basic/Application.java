package be.swing.basic;

import javax.swing.*;
import javax.swing.plaf.nimbus.NimbusLookAndFeel;

public class Application {

    public static void main(String[] args) throws Exception {
        UIManager.setLookAndFeel(new NimbusLookAndFeel());
        MainFrame MainFrame = new MainFrame();
        MainFrame.setVisible(true);
    }
}
