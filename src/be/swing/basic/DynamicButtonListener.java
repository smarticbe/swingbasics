package be.swing.basic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class DynamicButtonListener implements MouseListener {

    @Override
    public void mouseClicked(MouseEvent e) {
        JButton button = getButton(e);
        Container container = button.getParent();
        container.remove(button);
        container.repaint();
    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {
        getButton(e).setBackground(Color.RED);
    }

    @Override
    public void mouseExited(MouseEvent e) {
        getButton(e).setBackground(Color.LIGHT_GRAY);
    }

    private JButton getButton(MouseEvent e) {
        return (JButton)e.getSource();
    }
}
