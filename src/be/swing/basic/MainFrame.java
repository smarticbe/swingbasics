package be.swing.basic;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowAdapter;

public class MainFrame extends JFrame {

    public MainFrame() {
        super("mon interface graphique");
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.setSize(new Dimension(600, 400));
        this.setLocationRelativeTo(null);
        this.setLayout(new BorderLayout());

        //-------------------------------------------------------------------------------
        // Components
        //-------------------------------------------------------------------------------
        JPanel northPanel = new JPanel();
        add(northPanel, BorderLayout.NORTH);

        JTextField textField = new JTextField(50);
        northPanel.add( textField);

        JButton buttonOk = new JButton("coder");
        northPanel.add( buttonOk );

        JButton buttonReset = new JButton("Reset");
        buttonReset.setVisible(false);
        northPanel.add(buttonReset);


        JPanel centerPanel = new JPanel();
        add(centerPanel, BorderLayout.CENTER);

        //-------------------------------------------------------------------------------
        // Listeners
        //-------------------------------------------------------------------------------
        buttonOk.addActionListener(e -> {
            String utiText = textField.getText();
            textField.setEditable(false);
            buttonReset.setVisible(true);
            buttonOk.setVisible(false);
            utiText.chars()                                     // chars as integer
                    .mapToObj(c -> String.valueOf((char)c))     // convert each element into string
                    .forEach(s -> addButton(s, centerPanel));   // perform for each string
        });
        buttonReset.addActionListener(e -> {
            System.out.println("Remove all the dynamic buttons");
            centerPanel.removeAll();
            centerPanel.repaint();
            textField.setEditable(true);
            buttonReset.setVisible(false);
            buttonOk.setVisible(true);
        });
        textField.addKeyListener(new MyKeyListener());


        //Partie pour fermer l'app
        this.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent event) {
                int clickedButton =
                        JOptionPane.showConfirmDialog(
                                MainFrame.this,
                                "Etes-vous certains de vouloir quitter ?","Sortir de l'app",
                                JOptionPane.YES_NO_OPTION);
                if ( clickedButton == JOptionPane.YES_OPTION){
                    dispose();
                }

            }
        });
    }

    public void addButton(String name, JPanel panel) {
        System.out.println("Add button '" + name + "'");
        JButton button = new JButton(name);
        button.addMouseListener(new DynamicButtonListener());
        panel.add(button);
        panel.revalidate();
    }
}
